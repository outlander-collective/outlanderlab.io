---
title: Getting Connected
subtitle: First time setup
comments: false
weight: 3
---

## Whitelisting

To get whitelisted, ask @iris - `@iris minecraft add my_minecraft_name`. If that doesn't work or you don't have access to @iris, you can also ask VorpalHex.

## First time setup

1. Install the [Twitch Desktop Client](https://www.twitch.tv/downloads)
2. Go to the Mods tab
3. Select Minecraft
4. "Browse Modpacks"
5. Find "Enigmatica 2 Light" (For MC 1.12.2) and install it **Make sure to install the light version, not the regular one**
6. Click "Play" on Enigmatica 2 Light
7. Click "Play" on the Minecraft Launcher
8. Multiplayer -> Add Server
9. Name can be whatever you want, I recommend "Outlanders"
10. Host is "minecraft.outlander.group:25566"
11. Click save and connect

## Unable to connect

Try connecting a few times. Getting a timeout on initial connect is not uncommon unfortunately. If you continue to not be able to connect, contact an Admin to make sure you've been whitelisted.
